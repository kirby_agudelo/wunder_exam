<form novalidate class="needs-validation" action="" method="post">

<h2>Personal Information</h2>

<div class="input-group mb-3">
  <input type="text" name="firstname" class="form-control" placeholder="First Name" aria-label="firstname" required>


  <div class="invalid-feedback">
        Please provide a first name
  </div>

</div>


<div class="input-group mb-3">
  
  <input type="text" name="lastname" class="form-control" placeholder="Last Name" aria-label="lastname" required>

  <div class="invalid-feedback">
        Please provide a last name
  </div>


</div>


<div class="input-group mb-3">
  
  <input type="text" name="telephone" class="form-control" placeholder="Telephone" aria-label="telephone" required>
  <div class="invalid-feedback">
        Please provide a telephone number
  </div>


</div>


<button type="submit" class="btn btn-primary" name="p_submit">Next</button>
</form>