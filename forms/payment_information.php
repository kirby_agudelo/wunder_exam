<form novalidate class="needs-validation" method="post" action="">


<h2>Payment Information</h2>

<div class="input-group mb-3">
    <input type="text" class="form-control"  name ="acc_owner"  placeholder="Account Owner" aria-label="accouont_owner" required>

    <div class="invalid-feedback">
        Please provide a value for Account Owner
  </div>

</div>

<div class="input-group mb-3">
    <input type="text" class="form-control"  name ="iban"  placeholder="IBAN" aria-label="iban" required>
    <div class="invalid-feedback">
        Please provide a value for IBAN
  </div>
</div>

<button type="submit" class="btn btn-primary"  name ="py_submit"  >Next</button>
</form>