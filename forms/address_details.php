<form novalidate class="needs-validation" method="post" action="">


<h2>Address Information</h2>

<div class="input-group mb-3">
   <input type="text" class="form-control"  name ="house_no"  placeholder="House Number" aria-label="housenumber">
</div>


<div class="input-group mb-3">
  <input type="text" class="form-control"  name ="street"  placeholder="Street" aria-label="street">
</div>


<div class="input-group mb-3">
  <input type="text" name ="zipcode" class="form-control" placeholder="Zip Code" aria-label="zipcode" required>
  <div class="invalid-feedback">
        Please provide a value for Zip Code
  </div>
</div>


<div class="input-group mb-3">
  <input type="text" class="form-control"  name ="city"  placeholder="City" aria-label="city" required>
  <div class="invalid-feedback">
        Please provide a value for City
  </div>
</div>

<button type="submit" class="btn btn-primary"  name ="a_submit"  >Next</button>
</form>