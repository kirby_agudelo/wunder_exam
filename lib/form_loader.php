<?php 

// $customer_object is derived from request handler

if( ! $customer_object  ){
    $load = 1;
}
elseif( $customer_object->first_name && $customer_object->last_name && $customer_object->telephone ){
    $load = 2;

    if( $customer_object->city && $customer_object->zip ){
        $load = 3;


        if( $customer_object->account_owner &&  $customer_object->iban ){
            $load = 4;
        }

    }
}
else{
    $load = 1;
}


// condition for default page

if($load == 1 ){
    include("../forms/personal_details.php");

}
elseif( $load == 2 ){
    include("../forms/address_details.php");
}

elseif( $load == 3 ){
    include("../forms/payment_information.php");
}

elseif( $load == 4 ){
    // success page
    include("../forms/success_page.php");
}