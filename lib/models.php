<?php
namespace Models ;



class TempData {

    public $temp_id ;

    public function __construct( $temp_id ){

        $this->temp_id = $temp_id;

    }

}

class CustomerData extends TempData {

    public $customer_id;
    public $first_name;
    public $last_name;
    public $telephone ;

    public $street ;
    public $house_no;
    public $zip;
    public $city;

    public $account_owner ;
    public $iban ;
    public $payment_data_id;

}


class PersonalInformation extends CustomerData {



    public function __construct( $first_name, $last_name, $telephone, $temp_id ){

        parent::__construct($temp_id);

        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->telephone = $telephone ;


    }

}


class AddressInformation extends CustomerData {



    public function __construct( $street, $house_no, $zip, $city, $temp_id ){

        parent::__construct($temp_id);

        $this->street = $street;
        $this->house_no = $house_no;
        $this->zip = $zip;
        $this->city = $city;

    }


}


class PaymentInformation extends CustomerData {


    public function __construct( $account_owner, $iban, $temp_id ){

        parent::__construct($temp_id);

        $this->account_owner = $account_owner;
        $this->iban = $iban ;

    }


}


class WunderApiResult {

    public $payment_data_id ;

    public function __construct( $payment_data_id ){
        $this->payment_data_id = $payment_data_id;
    }

}