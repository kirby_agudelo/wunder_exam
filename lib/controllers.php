<?php
namespace Controllers ;

require_once __DIR__ ."/models.php" ;

require_once __DIR__."/dao.php" ;

class AppController {

    public static function fetch_existing_temporary_info( $temporary_id ){

        $customer_info_data = \Dao\CustomerDao::fetch_existing_temp( $temporary_id );
        return  $customer_info_data ;

    }

    public static function fetch_final_customer( $customer_id ){

        return \Dao\CustomerDao::fetch_final_customer( $customer_id );

    }


    public static function save_personal_information( $first_name, $last_name, $telephone, $temp_id ){

        // validation here

        // make into an object

        $personal_details = new \Models\PersonalInformation( $first_name, $last_name, $telephone, $temp_id ) ;

        \Dao\CustomerDao::insert_temp_personal( $personal_details ) ;

    }


    public static function save_address_information( $street, $house_no, $zip, $city, $temp_id ){

        // validation here
        $address_details = new \Models\AddressInformation($street, $house_no, $zip, $city, $temp_id);

        \Dao\CustomerDao::save_address_details( $address_details ) ;

    }

    public static function cleanup_temporary_data( $temporary_id ){

        \Dao\CustomerDao::cleanup_temp( $temporary_id );


    }


    public static function save_payment_information( $account_owner, $iban, $temp_id  ){
        // extra validation here

        $payment_information = new \Models\PaymentInformation($account_owner, $iban, $temp_id);

        \Dao\CustomerDao::save_payment_information( $payment_information ) ;

    }


    public static function save_complete_customer_info( \Models\CustomerData $customer_information ){


        $customer_id = \Dao\CustomerDao::save_complete_customer_info($customer_information);

        // call api and get payment id
        $response = \Controllers\WunderDemoPaymentApiCaller::call_api($customer_information->customer_id, $customer_information->iban, $customer_information->account_owner);

        if($response){
            $customer_information->payment_data_id = $response->payment_data_id;

            \Dao\CustomerDao::save_final_payment_info($customer_information);

        }       
    }
}



class WunderDemoPaymentApiCaller  {

    const API_URL = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    public static function call_api(  $customer_id, $iban, $owner ) {

        $data_string = self::prepare_request_body( $customer_id, $iban, $owner );
        
        $defaults = array(
            CURLOPT_URL => self::API_URL,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data_string,
            CURLOPT_RETURNTRANSFER => true
        );

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);

        $response = curl_exec($ch);

        if (!curl_errno($ch)) {
            $info = curl_getinfo($ch) ;
        
            if( $info['http_code'] == '200' ){
                
                $respopnse_object = json_decode($response);

                return new \Models\WunderApiResult($respopnse_object->paymentDataId);

            }
            else{
                //print "api request error\n\n";
            }

        }
        else{
            //print "CURL ERROR";
        }

    }


    public static function prepare_request_body( $customer_id, $iban, $owner ){

        return json_encode(["customerId"=> $customer_id, "iban"=> $iban, "owner" => $owner]);

    }

}