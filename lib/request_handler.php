<?php
session_start();

require_once __DIR__ ."/../config.php";

require_once __DIR__ . "/controllers.php";

// using session id, fetch if data in temporary table exists
$temporary_id = session_id();
if( isset($_SESSION['customer_id']) ){
    $customer_object = \Controllers\AppController::fetch_final_customer($_SESSION['customer_id']);
}
else{
    $customer_object = \Controllers\AppController::fetch_existing_temporary_info( $temporary_id );
}



/**
 * handle saving of personal information
 */
if( isset($_POST['p_submit'])){

    $first_name = $_POST['firstname'];
    $last_name = $_POST['lastname'] ;
    $telephone = $_POST['telephone'] ;
    $temp_id = session_id();
    \Controllers\AppController::save_personal_information( $first_name, $last_name, $telephone , $temp_id);
    header('Location: '.$url_base_path);
    die();

}
/**
 * handle saving of address information
 */
elseif(isset($_POST['a_submit'])){

    $street = $_POST['street'];
    $house_no = $_POST['house_no'];;
    $zip = $_POST['zipcode'];;
    $city = $_POST['city'];;
    $temp_id = session_id();

    \Controllers\AppController::save_address_information( $street, $house_no, $zip, $city, $temp_id );
    header('Location: '.$url_base_path);
    die();
}
/**
 * handle saving of payment information
 */
elseif( isset($_POST['py_submit'])){

    $account_owner = $_POST['acc_owner'];
    $iban = $_POST['iban'];
    $temp_id = session_id();

    \Controllers\AppController::save_payment_information($account_owner, $iban, $temp_id );

    $customer_object->account_owner = $account_owner;
    $customer_object->iban =$iban ;

    \Controllers\AppController::save_complete_customer_info($customer_object);

    // cleanup temp data
    \Controllers\AppController::cleanup_temporary_data( $temp_id );
    
    $_SESSION['payment_id'] = $customer_object->payment_data_id;
    $_SESSION['customer_id'] = $customer_object->customer_id;
    header('Location: '.$url_base_path);
    die();


}

$db_conn->close();