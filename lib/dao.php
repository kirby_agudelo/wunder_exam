<?php
namespace Dao ;
require_once __DIR__ ."/models.php";

class CustomerDao {

    const TBL_PERSONAL_TMP = 'reg_details_temporary';
    const TBL_PERSONAL = 'reg_details';
    const TBL_PAYMENT = 'payment_info';

    public static function fetch_final_customer( $customer_id ){

        global $db_conn ;

        $query = "SELECT r.firstname, r.lastname, r.telephone, r.addr_house_no, r.addr_street, r.addr_zip_code, r.addr_city,
        p.IBAN, p.account_owner, p.payment_data_id
        FROM reg_details r
        JOIN payment_info p ON p.customer_id = r.id
        WHERE r.id={$customer_id}";

        $result = $db_conn->query( $query );

        if(  $result && $result->num_rows > 0 ){

            $customer_data = new \Models\CustomerData( null );

            while( $row = $result->fetch_assoc() ){
                $customer_data->first_name = $row['firstname'];
                $customer_data->last_name = $row['lastname'];
                $customer_data->telephone = $row['telephone'];
                $customer_data->street = $row['addr_street'];
                $customer_data->house_no = $row['addr_house_no'];
                $customer_data->zip = $row['addr_zip_code'];
                $customer_data->city = $row['addr_city'];
                $customer_data->account_owner = $row['account_owner'];
                $customer_data->iban  = $row['IBAN'];
                $customer_data->payment_data_id = $row['payment_data_id'];

                break;
            }

            return $customer_data;

        }

    }

    public static function save_final_payment_info( \Models\CustomerData $customer_information ){
        $data = [
            "customer_id" => $customer_information->customer_id,
            "IBAN" => $customer_information->iban,
            "account_owner" => $customer_information->account_owner,
            "payment_data_id" => $customer_information->payment_data_id
        ];
        
        $id = self::insert_data( self::TBL_PAYMENT, $data );
        
    }

    public static function save_complete_customer_info( \Models\CustomerData $customer_information ){

        $data = [
            "firstname" => $customer_information->first_name ,
            "lastname"  => $customer_information->last_name, 
            "telephone" => $customer_information->telephone,

            "addr_house_no" => $customer_information->house_no,
            "addr_street" => $customer_information->street,
            "addr_zip_code" => $customer_information->zip  ,
            "addr_city" => $customer_information->city

        ];

        $id = self::insert_data( self::TBL_PERSONAL, $data );


        $customer_information->customer_id = $id;
        
        return $id;

    }


    public static function cleanup_temp( $temp_id ){

        global $db_conn ;

        $table = self::TBL_PERSONAL_TMP;

        $hash_id = sha1($temp_id);

        $query = "DELETE FROM {$table} where `temporary_id`= '{$hash_id}' LIMIT 1";

        $db_conn->query( $query );

    }

    public static function fetch_existing_temp( $temp_id ){

        global $db_conn ;

        $table = self::TBL_PERSONAL_TMP ;
        
        $hashed_id = sha1($temp_id);

        $query ="SELECT `temporary_id`, `firstname`, `lastname`, `telephone`, `addr_house_no`, `addr_street`, `addr_zip_code`, `addr_city`, `IBAN`, `account_owner`, `payment_data_id` FROM `{$table}` WHERE `temporary_id`='{$hashed_id}'";

        $result = $db_conn->query( $query );

        if(  $result && $result->num_rows > 0 ){
            
            $customer_data = new \Models\CustomerData($temp_id);

            

            while( $row = $result->fetch_assoc() ){
                $customer_data->first_name = $row['firstname'];
                $customer_data->last_name = $row['lastname'];
                $customer_data->telephone = $row['telephone'];
                $customer_data->street = $row['addr_street'];
                $customer_data->house_no = $row['addr_house_no'];
                $customer_data->zip = $row['addr_zip_code'];
                $customer_data->city = $row['addr_city'];
                $customer_data->account_owner = $row['account_owner'];
                $customer_data->iban  = $row['IBAN'];
                break;
            }

            return $customer_data;
        }

    }


    private static function __fetch_record( $table, $fields, $cond ){

    }


    public static function insert_temp_personal( \Models\PersonalInformation $details ){

        $data = [
            "firstname" => $details->first_name ,
            "lastname"  => $details->last_name, 
            "telephone" => $details->telephone,
            "temporary_id" => sha1($details->temp_id)

        ];

        self::insert_data( self::TBL_PERSONAL_TMP, $data );
    
    }
    

    public static function save_payment_information( \Models\PaymentInformation $details ){

        $data = [
            "IBAN" =>$details->iban,
            "account_owner" => $details->account_owner
        ];


        $temp_id = sha1($details->temp_id);
        $affected_rows = self::update_table(self::TBL_PERSONAL_TMP, $data, $temp_id);

        if( $affected_rows == 0 ){
            // @todo throw exception =====> no such temporary id (temporary user was erased)
            // needs more information on how to handle
        }

    }

    public static function save_address_details( \Models\AddressInformation $details ){

        $data = [
            "addr_house_no" => $details->house_no,
            "addr_street" => $details->street,
            "addr_zip_code" => $details->zip  ,
            "addr_city" => $details->city


        ];

        $temp_id = sha1($details->temp_id);
        $affected_rows = self::update_table(self::TBL_PERSONAL_TMP, $data, $temp_id);

        if( $affected_rows == 0 ){
            // @todo throw exception =====> no such temporary id (temporary user was erased)
            // needs more information on how to handle

        }

    }

    private static function update_table( $table, $data, $temp_id ){

        global $db_conn ;

        $field_data = [];

        foreach($data as $field => $new_value){
            $new_value = addslashes($new_value);
            $field_data[] = "`{$field}`='{$new_value}'";
        }

        $field_data_str = implode(",", $field_data);

        $query ="UPDATE `{$table}` SET {$field_data_str} WHERE `temporary_id`='{$temp_id}'";

        $result = $db_conn->query( $query );

        return $db_conn->affected_rows ;

    }
    
    
    private static function insert_data( $table, $data ){

        global $db_conn ;

        $fields= [];
        $values= [];

        $field_str = "";
        $value_str = "";

        foreach( $data as $field => $value ){
            $fields[] = "`{$field}`";
            $value = addslashes($value);
            $values[] = "'{$value}'";
        }

        $field_str = implode(",", $fields);
        $value_str = implode(",", $values);

        $query = "INSERT INTO `{$table}`($field_str) VALUES({$value_str})";

        $result = $db_conn->query( $query );

        return $db_conn->insert_id;
    
    }
}