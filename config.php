<?php
/**
 * application written by Vincent Agudelo my.unknown@gmail.com
 */
// set error level
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$url_base_path  = "/";

// database configuration
$__db_config = [
        "host" => "localhost",
        "database" => "wunder_registration",
        "username" => "wunderuser",
        "password" => "testpassword"
        ] ;

$db_conn = @new mysqli( $__db_config["host"],  $__db_config["username"] , $__db_config["password"], $__db_config["database"]);

if ($db_conn->connect_errno) {
    // mysql cannot be reached
    include( __DIR__ .'/public/no_service.php' );
    die();
}