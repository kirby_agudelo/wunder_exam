# HTTP Server Config

Set your DocumentRoot to the **/public** directory. For example, if the files are copied to a directory named `wunder`  under `/var/www/html`:

```
/var/www/wunder/
```

Set the DocumentRoot of the http server to the following

```
/var/www/html/wunder/public
```


# Mysql Tables

These are the tables to be used. **reg_details** is where the data are to be permanently saved.

```sql
create table `reg_details`(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `firstname` VARCHAR(16) NOT NULL,
    `lastname` VARCHAR(16) NOT NULL,
    `telephone` VARCHAR(16) NOT NULL,
    `addr_house_no` VARCHAR(12) NOT NULL,
    `addr_street` VARCHAR(32) NOT NULL,
    `addr_zip_code` VARCHAR(8) NOT NULL,
    `addr_city` VARCHAR(32) NOT NULL
);
```

**payment_info** is where the payment details are saved. The tables are separated just for safety.
```sql
create table `payment_info`(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `customer_id` INT UNIQUE NOT NULL, -- foreign key
    `IBAN` VARCHAR(8) NOT NULL,
    `account_owner` VARCHAR(32) NOT NULL,
    `payment_data_id` VARCHAR(128) NULL
);
```

**reg_details_temporary** is where data is saved temporarily while the user is still in the process of saving data. At the end of the transaction the customer data is deleted. This will prevent too many UPDATE statements in the main (final) table.

```sql
create table `reg_details_temporary`(
`temporary_id` varchar(40) NOT NULL, -- temporary id , will use sha1 of the session id since session id can vary in length
`firstname` VARCHAR(16) DEFAULT NULL,
`lastname` VARCHAR(16) DEFAULT NULL,
`telephone` VARCHAR(16) DEFAULT NULL,
`addr_house_no` VARCHAR(12) DEFAULT NULL,
`addr_street` VARCHAR(32) DEFAULT NULL,
`addr_zip_code` VARCHAR(8) DEFAULT NULL,
`addr_city` VARCHAR(8) DEFAULT NULL,
`IBAN` VARCHAR(8) DEFAULT NULL,
`account_owner` VARCHAR(32) DEFAULT NULL,
`payment_data_id` VARCHAR(128) DEFAULT NULL,
UNIQUE(`temporary_id`)
);
```

## Test User for Database / Permissions

Setting separate permissions on the tables will add some level of safety when using the tables.

```sql
GRANT USAGE ON `wunder_registration`.* TO 'wunderuser'@'localhost' IDENTIFIED BY 'testpassword';

GRANT SELECT,INSERT,UPDATE, DELETE
ON `wunder_registration`.`reg_details_temporary`
TO 'wunderuser'@'localhost' IDENTIFIED BY 'testpassword';

GRANT SELECT,INSERT,UPDATE
ON `wunder_registration`.`payment_info`
TO 'wunderuser'@'localhost';

GRANT SELECT,INSERT
ON `wunder_registration`.`reg_details`
TO 'wunderuser'@'localhost';
```
