<?php
require_once "config.php";

require_once __DIR__ . "/../lib/controllers.php";



function test_insert_personal_data(){


    $first_name = "test first name";
    $last_name = "test last name" ;
    $telephone = "test telephone" ;

    \Controllers\AppController::save_personal_information( $first_name, $last_name, $telephone , "temporary id1");


}



function save_address_details(){

    $street = "test street";
    $house_no = "12'34";
    $zip = "43bg";
    $city = "my city";
    $temp_id = "temporary id1";

    \Controllers\AppController::save_address_information( $street, $house_no, $zip, $city, $temp_id );

}


function save_payment_details(){
    $account_owner = "i am the owner";
    $iban = "iban123";
    $temp_id = "temporary id";

    \Controllers\AppController::save_payment_information($account_owner, $iban, $temp_id );


}


function fetch_customer_information(){

    $temporary_id = "temporary id";
    \Controllers\AppController::fetch_existing_temporary_info( $temporary_id );

}


function test_api_call(){


    $response = \Controllers\WunderDemoPaymentApiCaller::call_api(1, "DE8234", "Max Mustermann");


    print_r($response);

}


function copy_to_real(){

    $temporary_id = "1hcct48maebk7slp1keme0kek1";
    $customer_object = \Controllers\AppController::fetch_existing_temporary_info( $temporary_id );
    print_r($customer_object);
    $customer_id = \Controllers\AppController::save_complete_customer_info($customer_object);

    //print "insert id: {$customer_id}\n\n";

}

function dao_save_payment(){


    $temporary_id = "1hcct48maebk7slp1keme0kek1";
    $customer_object = \Controllers\AppController::fetch_existing_temporary_info( $temporary_id );
    $customer_object->customer_id = 1;
    \Dao\CustomerDao::save_final_payment_info($customer_object);

}


function dao_fetch_final(){
    $customer_id = 1;
    $data = \Dao\CustomerDao::fetch_final_customer( $customer_id );
    print_r($data);

}



//test_insert_personal_data();
//save_address_details();
// save_payment_details();

//fetch_customer_information();

//test_api_call();
// copy_to_real();
//dao_save_payment();

dao_fetch_final();